<?php

/**
 * Null dog : equivalent with the single quote
 * hear dog : equivalent with double quote
 */
$myVariable = "Lovely";

$myString = 'Hello $myVariable World';  //null dog example

$myString = "Hello $myVariable World"; //hear dog example

//null dog, if I omit the sigle quote It'll be formed as hear dog
$myString = <<<'BITM'
<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
<meta charset="utf-8">
<title>Trilliant Diamond | Gold and Diamond Shop</title>
<!-- SEO Meta Tags-->
<meta name="keywords" content="Trilliant Diamond">
<meta name="description" content="Trilliant Diamond">
<meta name="author" content="Trilliant Diamond">
<meta name="distribution" content="web">
<meta name="robots" content="noindex, nofollow">

<link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@200;300;400;600;700;800;900&display=swap" rel="stylesheet preload">

<!-- Mobile Specific Meta Tag-->
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<!-- Favicon Icons-->
<link rel="icon" type="image/png" href="images/trilian_logo2.png">
<link rel="apple-touch-icon" href="images/trilian_logo2.png">
<link rel="apple-touch-icon" sizes="152x152" href="images/trilian_logo2.png">
<link rel="apple-touch-icon" sizes="180x180" href="images/trilian_logo2.png">
<link rel="apple-touch-icon" sizes="167x167" href="images/trilian_logo2.png">
<!-- Vendor Styles including: Bootstrap, Font Icons, Plugins, etc.-->
<link rel="stylesheet" media="screen" href="assets/front/css/plugins.min.css">
<link id="mainStyles" rel="stylesheet" media="screen" href="assets/front/css/styles.min.css?v=30">
<link id="mainStyles" rel="stylesheet" media="screen" href="assets/front/css/responsive.css">
<!-- Color css -->
<link href="assets/front/css/color6e7b.css?primary_color=FF6A00" rel="stylesheet">
<!-- Modernizr-->
<script src="assets/front/js/modernizr.min.js"></script>
<style>
.left-category-area .category-header h4,.menu-top-area,.mm-heading-area{
    background: #d38195 !important;
   }
.btn-primary,.footer-social-links a,.widget-title::after,.btn,.a2a_kit a,.product-card .product-button-group .product-button{
  background-color: #955570 !important;
}
.product-card:hover, .brand-slider .slider-item a:hover, .genius-banner:hover {
    border-color: #d38195;
}
.business-name{
color: #008d89;
}

.site-header .site-menu > ul > li:hover > a{
    color: #006f6e !important;
    font-weight: 600;
}
.mainTitle{
    color: #008175 !important;
}

.hero-slider .owl-carousel .owl-nav div, .left-category-area .category-list .navi-link:hover span.text-gray-dark, .site-header .navbar .nav-inner .right-info i, .h-t-social-area ul li a:hover, .menu-top-area .login-register:hover, .t-h-dropdown a:hover, .t-h-dropdown a.active, .product-card .product-price, .genius-banner .content .content-inner p, .navi-link:hover, .site-header .site-menu > ul > li:hover > a, .widget-categories ul > li.active > a, .widget-links ul > li.active > a, .details-page-top-right-content  a, .widget-categories ul > li > a:hover, .widget-links ul > li > a:hover, .product-card .product-title > a:hover, .product-card .product-category > a:hover, .nav-tabs .nav-link:hover, .post-title > a:hover, .post-meta > li > a:hover, .widget-featured-posts > .entry .entry-title > a:hover, .widget-featured-products > .entry .entry-title > a:hover, .widget-cart > .entry .entry-title > a:hover, .entry .entry-delete a, .steps .step.active .step-title, .steps .step.active > i, .text-primary, .shopping-cart .product-item .product-title > a:hover, .wishlist-table .product-item .product-title > a:hover, .order-table .product-item .product-title > a:hover, .list-group-item.active, a.list-group-item:hover, a.list-group-item:focus, a.list-group-item:active, .list-group-item-action:hover, .list-group-item-action:focus, .list-group-item-action:active, .progress-steps li.active .icon, .comparison-table .comparison-item .comparison-item-title:hover, .site-header .site-menu > ul > li.active > a, .breadcrumbs > li > a:hover, .faq-box:hover .link, .left-category-area .category-list .sub-c-box .title:hover, .left-category-area .category-list .sub-c-box .child-category a:hover, .section-title .links a:hover, .section-title .links a.active, #quick_filter li a:hover, #quick_filter li a.active, .section-title .right_link:hover, .popular-category.theme3 .links a.active, .popular-category.theme3 .links a:hover, .site-header .search-box-wrap .input-group .serch-result .bottom-area a:hover, .shop-view>a, .genius-banner .inner-content p, .details-page-top-right-content .price-area .main-price, .free-shippin-aa {
    color: #005654 !important;
}
.pagination li a, .pagination li span {
background: #FFD1DC !important;
font-size: 12px;
}
 .site-header .toolbar .toolbar-item > a > div > .cart-icon > .count-label{
    background-color: #FFD1DC !important;
}

.site-header .topbar {
	background: #FFD1DC;
}

.site-footer {
    background-color: #FFD1DC;
}body {
	background-color: #fff !important;
	font-family: Helvetica,sans-serif;
}
/*
{
    font-family: Nunito Sans,sans-serif!important;
}*/

.site-header .site-menu>ul>li>form>button {
    background: none;
    border: none;
    padding:13px 30px;
}
.widget-categories ul>li>form>button {
    background: none;
    border: none;
    padding: 0 !important;
}

.site-header .site-menu ul>li>a {
    color: #000;
}
.site-header .topbar {
	border-bottom: none;
}
.site-header .site-menu>ul {
	display: flex;
	justify-content: space-between;
}
.slider-area-wrapper {
    margin-top: 0;
}
@media only screen and (min-width: 768px) {
	.hero-slider .item {
		height:768px;
	}
}
header .search-input {
    -webkit-appearance: none;
    appearance: none;
    background-clip: padding-box;
    background-color: #fff;
    border: none;
    color: #000;
    display: block;
    font-size: 14px;
    margin-left: 10px;
    padding: 4px;
    text-overflow: ellipsis;
    transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
    width: 100%;
}
.form-control:focus {
	background-color: #fff;
}
/* start code for mega menu */
.site-header .site-menu>ul>li:hover .sub-menu {
    background-color: #fff;
    border-radius: 5px;
    box-shadow: 0 3px 20px rgba(131,39,41,.8);
    display: block;
    left: -0px;
    padding: 20px 40px;
    position: absolute;
    white-space: nowrap;
    width: -webkit-max-content;
    width: max-content;
    z-index: 2;
}
/* end code for mega menu */

/* start home page login */
.menu-account:hover .tqAccountPop{
	opacity: 1;
	/* -webkit-transform: translate(-50%,-4px); */
	visibility: visible;
}
.tqAccountPop {
    background: #fff;
    border-radius: 5px;
    box-shadow: 0 3px 20px rgba(131,39,41,.8);
    display: block;
    left: 50%;
    min-width: 280px;
    opacity: 0;
    padding: 20px;
    position: absolute;
    text-align: center;
    top: 81px;
    -webkit-transform: translate(-50%,40px);
    transform: translate(-50%,40px);
    transition: all .3s;
    visibility: hidden;
    z-index: 20;
}
/* end home page login */

#common-button .secondary-btn:hover {
    border: .01em solid #631617;
    color: #631617;
}
#common-button .primary-btn {
    background-color: #832729 !important;
    border: .01em solid #832729 !important;
    border-radius: 5px;
    box-shadow: none;
    color: #000;
    color: #fff !important;
    cursor: pointer;
    font-size: 14px;
    font-weight: 800;
    height: 50px;
    line-height: 1em;
    outline: 0;
    padding: .875em 1.875em;
    position: relative;
    text-align: center;
    text-decoration: none;
    transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
    -webkit-user-select: none;
    -ms-user-select: none;
    user-select: none;
    vertical-align: middle;
    width: 100%;
}
#common-button .secondary-btn {
    background-color: #fff !important;
    border: .01em solid #832729 !important;
    border-radius: 5px;
    box-shadow: none;
    color: #000;
    color: #832729 !important;
    cursor: pointer;
    font-size: 14px;
    font-weight: 800;
    height: 50px;
    line-height: 1em;
    outline: 0;
    padding: .875em 1.875em;
    position: relative;
    text-align: center;
    text-decoration: none;
    transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
    -webkit-user-select: none;
    -ms-user-select: none;
    user-select: none;
    vertical-align: middle;
    width: 100%;
}
#common-button .primary-btn:hover {
    background-color: #631617 !important;
    color: #fff;
    outline: 0;
}
.tqAccountPopCTA {
    align-items: center;
    display: flex;
    justify-content: space-around;
    margin: 20px 0 10px;
}
.tqAccountPopCTA .btn {
    font-size: 12px!important;
    font-weight: unset!important;
    height: 40px!important;
    padding: 4px 8px;
    text-transform: uppercase;
}
.hr-vector {
    width: calc(100% + 8px);
    height: 40px;
    padding: 10px 4px;
    position: relative;
    margin: 10px 0;
    margin-left: -4px;
    margin-right: -4px;
    clear: both;
}
.hr-vector::before {
    content: '';
    position: absolute;
    width: 100%;
    height: 100%;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    background-image: url(assets/images/home-page-image/Line-Design.svg?v=01);
    background-position: center;
    background-repeat: no-repeat;
    background-size: 104%;
}
.product-card {
    border: 1px solid rgba(38,191,191,.1);
}
.product-card:hover {
	border-color: #F8C8DC;
	box-shadow: 		rgba(128,128,128, 0.24) 0px 3px 6px, rgba(128,128,128, 0.54) 0px 3px 6px;
	-webkit-box-shadow: rgba(128,128,128, 0.24) 0px 3px 6px, rgba(128,128,128, 0.54) 0px 3px 6px;
	-moz-box-shadow:    rgba(128,128,128, 0.24) 0px 3px 6px, rgba(128,128,128, 0.54) 0px 3px 6px;
    z-index: 2;
}
.owl-item {
	margin-bottom: 15px;
}
.video-card video {
	border-radius:26px;
}
.video-card video:hover {
    box-shadow: #02716f 0px 3px 6px, #02716f 0px 3px 6px;
}

.request-button {
    padding: 0 10px 0 10px;
    font-weight: 600;
    z-index: 9999;
    cursor: pointer;
    background-color: #ff7898;
    border-top: 1px solid #ff7898;
    color: #fff;
    display: inline-block;
    position: fixed;
    right: 13px;
    height: 30px;
    line-height: 29px;
    font-size: 14px;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    border-radius: 3px;
}
.breadcrumbs {
	padding-top: 0;
}
</style>

<style>
.topbar {
	background-image: url('./assets/top_banner.png?v=05') !important;
	background-size:cover !important;
	min-height:70px !important;
}
.topbar .site-branding {
	width: 100%;
}
.topbar .container {
	max-width: 97%;
}

.top-1 {
    top: 24.5%;
}
.top-2 {
    top: 28%;
}
@media only screen and (max-width: 991px) {
	.topbar {
		background-image: url('./assets/top_banner_mobile.png?v=03') !important;
		min-height:100px !important;
	}
	.topbar .site-branding {
		width: 90%;
	}
	.topbar .container {
		max-width: 100%;
	}
	.top-1 {
		top: 233px;
	}
	.top-2 {
		top: 266px;
	}
}

</style>
</head>
<!-- Body-->
<body class="body_theme1">
<!-- Preloader Start -->
<div id="preloader">
    <img src="assets/images/loader.gif" alt="Loading...">
</div>
<header class="site-header navbar-sticky">
			<!--
<div class="menu-top-area">
   <div class="container">
      <div class="row">
         <div class="col-md-4">
            <div class="t-m-s-a">
               <a class='track-order-link' href='login.php'><i class='icon-map-pin'></i>Login</a>              <=!-- <a class="track-order-link compare-mobile d-lg-none" href="compare/products.html">Compare</a>--=>
            </div>
         </div>
         <div class="col-md-8">
            <div class="right-area">
              <=!-- <a class="track-order-link wishlist-mobile d-inline-block d-lg-none" href="user/login.html"><i class="icon-heart"></i>Wishlist</a>
               <div class="t-h-dropdown ">
                  <a class="main-link" href="#">Currency<i class="icon-chevron-down"></i></a>
                      <div class="t-h-dropdown-menu">
                              <a class="active" href="index.html"><i class="icon-chevron-right pr-2"></i>USD</a>
                              <a class="" href="index.html"><i class="icon-chevron-right pr-2"></i>EUR</a>
                              <a class="" href="index.html"><i class="icon-chevron-right pr-2"></i>INR</a>
                              <a class="" href="index.html"><i class="icon-chevron-right pr-2"></i>BDT</a>
                              <a class="" href="index.html"><i class="icon-chevron-right pr-2"></i>NGN</a>
                      </div>
                </div>--=>
               <div class="login-register ">
                                 
               </div>
               
            </div>
         </div>
      </div>
   </div>
</div>
-->


<div class="topbar" style="position: relative;">
   <div class="container">
      <div class="row hidden-lg-up"> <!--  style="border-bottom: 1px solid #f9b3c3;" -->
         <div class="col-lg-12">
            <div class="d-flex justify-content-between">
               <!-- Logo-->
               <!--<div class="site-branding hidden-md-down" style="padding:0;margin: 0 auto">
                  <a class="site-logo align-self-center" href="index.php">
                     <img src="images/trilian_logo2.png" style="width:100px" alt=""></a>
				</div>-->
				<div class="site-branding hidden-lg-up" style="padding:0;">
					<a class="site-logo align-self-center" href="index.php" style="float:left">
                     <img src="images/trilian_logo2.png" style="width:100px" alt=""></a>
					<a class="site-logo align-self-center" href="index.php" style="float:right;width:auto">
                     <img src="images/jbt_logo.png?v=06" style="width:85px" alt=""></a>
				</div>
				
				<!--
				<div class="site-branding hidden-lg-up" style="padding:0;width: 100%">
					<a class="site-logo align-self-center" href="index.php" style="float:left">
                     <img src="images/trilian_logo2.png" style="width:100px" alt=""></a>
					<a class="site-logo align-self-center" href="index.php" style="float:right">
                     <img src="images/jbt_logo.png" style="width:100px" alt=""></a>
				</div>-->
                          
                <!-- 
               <div class="search-box-wrap d-none d-lg-block d-flex">
                  <div class="search-box-inner align-self-center">
                     <div class="search-box d-flex">
						<select name="category" id="category_select" class="categoris">
                           <option value="">All</option>
                                                   </select><form class="input-group" id="header_search_form" action="#" method="GET">
                           <input type="hidden" name="category" value="" id="search__category">
                           <span class="input-group-btn">
                           <button type="submit"><i class="icon-search"></i></button>
                           </span>
                           <input class="form-control search-input" type="text" data-target="#" id="" name="searchKey" placeholder="Search for Gold Jewellery, Diamond Jewellery and more…">
                           <div class="serch-result d-none">
                           </div>
                        </form>
                     </div>
                  </div>
                  <span class="d-block d-lg-none close-m-serch"><i class="icon-x"></i></span>
               </div>-->
               
               <!-- Toolbar-->
               <div class="toolbar d-flex">
                  <!--<div class="toolbar-item close-m-serch visible-on-mobile">
                     <a href="#">
                        <div>
                           <i class="icon-search"></i>
                        </div>
                     </a>
                  </div>-->
                  <div class="toolbar-item visible-on-mobile mobile-menu-toggle">
                     <a href="#">
                        <div><i class="icon-menu"></i><span class="text-label">Menu</span></div>
                     </a>
                  </div>
				  <!--
				  <div class="toolbar-item hidden-on-mobile menu-account">
						<a href="#">
						  <div>
							  <i class="icon-user"></i>
							  <span class="text-label">Account</span>
						  </div>
						</a>
                                       <div class="tqAccountPop">
                        <h4>My Account</h4>
                        <h6>Access your account</h6>
                        <div class="tqAccountPopCTA">
                       
                           <div id="common-button"><a  href="#" data-bs-toggle="modal" data-bs-target="#customerLoginModal"><button class="btn secondary-btn"><span class="">Log In</span></button></a></div>
                           <div id="common-button"><a href="#" data-bs-toggle="modal" data-bs-target="#customerRegistrationModal"><button class="btn primary-btn"><span class="">Sign Up</span></button></a></div>
                        </div>                        
                     </div>
                  				  </div>
				  
                 
                     	

	
                  <div class="toolbar-item">
                     <a href="cart_list.php">
                        <div><span class="cart-icon"><i class="icon-shopping-cart"></i><span class="count-label cart_count">0 </span></span><span class="text-label">Cart</span></div>
                     </a>
                     <div class="toolbar-dropdown cart-dropdown widget-cart  cart_view_header" id="header_cart_load" data-target="cart.php?show=topbar">
                       Cart empty
                     </div>
                  </div>
				-->          
               </div>
            </div>
			
			
				
			
            <!-- Mobile Menu-->
            <div class="mobile-menu">
               <!-- Slideable (Mobile) Menu-->
               <div class="mm-heading-area">
                  <h4>CATEGORIES</h4>
                  <div class="toolbar-item visible-on-mobile mobile-menu-toggle mm-t-two">
                     <a href="#">
                        <div> <i class="icon-x"></i></div>
                     </a>
                  </div>
               </div>
               <ul class="nav nav-tabs" role="tablist">                  
                  <li class="nav-item" role="presentation99">
                     <span class="active" id="mcat-tab" data-bs-toggle="tab" data-bs-target="#mcat" role="tab" aria-controls="mcat" aria-selected="false"></span>
                  </li>
               </ul>
               <div class="tab-content p-0">
					<div class="tab-pane fade active show" id="mcat" role="tabpanel" aria-labelledby="mcat-tab">
                     <nav class="slideable-menu">
                        <div class="widget-categories mobile-cat">
							<ul id="category_list">
								<!--
								<li><a href='index.php'>LOOSE LAB DIAMONDS</a></li>
								<li><a href='meleeList.php'>MELEE LAB DIAMONDS</a></li>
								<li><a href='pages.php?page=about-us'> ABOUT US</a></li>
								<li><a href='pages.php?page=shipping-policy'> SHIPPING POLICY</a></li>
								-->
								
								
<li class='main-menu'><a href='index.php'> LOOSE LAB DIAMONDS</a> </li>
<li class='main-menu'><a href='meleeList.php'> MELEE LAB DIAMONDS</a> </li>
<li><a class='subcategory' href='pages.php?page=about-us'> ABOUT US</a></li>
<li><a class='subcategory' href='payment.php'> PAYMENT</a></li>
<!--<li><a class='subcategory' href='pages.php?page=shipping-policy'> SHIPPING POLICY</a></li>-->

<!--<li>
<form action="https://www.paypal.com/cgi-bin/webscr" method="POST">
	<input type="hidden" name="payment_method" value="2">
	<input type="hidden" name="business" value="sam@trilliantdiam.com">
	<input type="hidden" name="no_shipping" value="1">
	<input type="hidden" name="currency_code" value="USD">
	<input type="hidden" name="notify_url" value="http://trilliantdiam.com/home/paypal/notify.php">
	<input type="hidden" name="cancel_return" value="http://trilliantdiam.com/home/paypal/cancel.php">
	<input type="hidden" name="return" value="http://trilliantdiam.com/home/paypal_return.php">
	<input type="hidden" name="cmd" value="_xclick">   
	<button class='subcategory' style="color:#505050;font-size:14px;" type="submit" name="pay_now" id="pay_now">PAYMENT</button>
</form>
</li>-->								
															   <!--
							   <li class='has-children'>  <a class='category_search' href='catalog.php?cat_id=5'>LOOSE DAIMONDS<span><i class='icon-chevron-down'></i></span></a>
								  <ul id='subcategory_list'>
									 <li class=''><a class='subcategory' href='catalog.php?attr_name=Shape&attr_val=Round&cat_id=5'> ROUND</a></li>
									 <li class=''><a class='subcategory' href='catalog.php?attr_name=Shape&attr_val=Oval&cat_id=5'> OVAL</a></li>
									 <li class=''><a class='subcategory' href='catalog.php?attr_name=Shape&attr_val=Cushion&cat_id=5'> CUSHION</a></li>
								  </ul>
							   </li>
							   <li class='has-children'><a class='category_search' href='catalog.php?cat_id=34'> RINGS <span><i class='icon-chevron-down'></i></span></a>
								  <ul id='subcategory_list'>
									<li class=''><a class='subcategory' href='catalog.php?attr_name=Shape&attr_val=Round&cat_id=34'> ROUND</a> </li>
									<li class=''><a class='subcategory' href='catalog.php?attr_name=Shape&attr_val=Oval&cat_id=34'> OVAL</a> </li>
									<li class=''><a class='subcategory' href='catalog.php?attr_name=Shape&attr_val=Cushion&cat_id=34'> CUSHION</a> </li>
									<li class=''><a class='subcategory' href='catalog.php?attr_name=Shape&attr_val=Square&cat_id=34'> SQUARE</a> </li>
									<li class=''><a class='subcategory' href='catalog.php?attr_name=Shape&attr_val=Marquise&cat_id=34'> MARQUISE</a> </li>
									<li class=''><a class='subcategory' href='catalog.php?attr_name=Shape&attr_val=Asscher&cat_id=34'> ASSCHER</a> </li>											
									<li class=''><a class='subcategory' href='catalog.php?attr_name=Shape&attr_val=Emerald&cat_id=34'> EMERALD</a> </li>											
									<li class=''><a class='subcategory' href='catalog.php?attr_name=Shape&attr_val=Pear shape&cat_id=34'> PEAR SHPAE</a> </li>											
								  </ul>
								</li>-->
						
							
							<!--<li class=''><a class='subcategory' href='pages.php?page=international_shipping'> TERM OF USE</a></li>-->
							   
							   							</ul>
                        </div>
                     </nav>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="row" style="padding: 0;">
		
		<!--<div class="col-lg-2 hidden-md-down">
		  <a class="site-logo align-self-center" href="index.php">
			 <img src="images/trilian_logo2.png" style="width:100px" alt=""></a>
		</div>-->
		<!--<div class="hidden-md-down" style="margin-top:15px">&nbsp;</div>-->
		
		<div class="site-branding hidden-md-down" style="padding:0;">
			<a class="site-logo align-self-center" href="index.php" style="float:left">
			 <img src="images/trilian_logo2.png" style="width:150px" alt=""></a>
			<a class="site-logo align-self-center" href="index.php" style="float:right;width:auto">
			 <img src="images/jbt_logo.png?v=06" style="width:100px" alt=""></a>
			<!--<div class="col-lg-12" style="position: absolute;bottom: 2px;text-align: center;width: 100%">
				<span style="font-size: 15PX;font-weight: 500;">TRILLIANT DIAMONDS LLC IS A JBT STANDARD MEMBER</span>
			</div>-->
		</div>
		
		<!--<div class="col-lg-12 hidden-lg-up" style="position: absolute;bottom: 0;text-align: center;width: 100%;">
			<span style="font-size: 13px;font-weight: 500;">TRILLIANT DIAMONDS LLC IS A JBT STANDARD MEMBER</span>
		</div>-->
		
	  </div>
   </div>
</div>

<div class="col-lg-12 hidden-md-down" style="text-align: center;">
	<span style="font-size: 19PX;font-weight: bold;">SHOP LOOSE LAB GROWN DIAMONDS AT WHOLESALE PRICES</span><br>
	<span style="font-size: 15PX;font-weight: 500;">MAKE YOUR OWN JEWELRY FOR FRACTION OF THE PRICE</span>
</div>

<div class="col-lg-12 hidden-lg-up" style="text-align: center;">
	<span style="font-size: 19PX;font-weight: bold;">SHOP LOOSE LAB GROWN DIAMONDS AT WHOLESALE PRICES</span><br>
	<span style="font-size: 15PX;font-weight: 500;">MAKE YOUR OWN JEWELRY FOR FRACTION OF THE PRICE</span>
</div>

<!-- Navbar-->
<div class="navbar">
	<div class="container">
		<div class="row g-3 w-100">
			<!--<div class="col-lg-3">
				<div class="left-category-area">
					<div class="category-header">
						<h4><i class="icon-align-justify"></i> Categories</h4>
					</div>
				</div>
			</div>-->
			<!--<div class="col-lg-12" style="text-align: center; padding-top: 21px;">
				<span style="font-size: 19PX;font-weight: bold;">SHOP LOOSE LAB GROWN DIAMOND</span><br>
				<span style="font-size: 15PX;font-weight: 500;">MAKE YOUR OWN RING FOR FRACTION OF THE PRICE</span><br>
				<span style="font-size: 14PX;font-weight: 500;color: gray;">All diamonds are GIA and IGI certified.</span>
			</div>-->
			<div class="col-lg-12 justify-content-between"> <!-- d-flex -->
				<div class="nav-inner">
					<nav class="site-menu">
						<ul>
												
<li class='main-menu'><a href='index.php'> LOOSE LAB DIAMONDS</a> </li>
<li class='main-menu'><a href='meleeList.php'> MELEE LAB DIAMONDS</a> </li>
<li><a class='subcategory' href='pages.php?page=about-us'> ABOUT US</a></li>
<li><a class='subcategory' href='payment.php'> PAYMENT</a></li>
<!--<li><a class='subcategory' href='pages.php?page=shipping-policy'> SHIPPING POLICY</a></li>-->

<!--<li>
<form action="https://www.paypal.com/cgi-bin/webscr" method="POST">
	<input type="hidden" name="payment_method" value="2">
	<input type="hidden" name="business" value="sam@trilliantdiam.com">
	<input type="hidden" name="no_shipping" value="1">
	<input type="hidden" name="currency_code" value="USD">
	<input type="hidden" name="notify_url" value="http://trilliantdiam.com/home/paypal/notify.php">
	<input type="hidden" name="cancel_return" value="http://trilliantdiam.com/home/paypal/cancel.php">
	<input type="hidden" name="return" value="http://trilliantdiam.com/home/paypal_return.php">
	<input type="hidden" name="cmd" value="_xclick">   
	<button class='subcategory' style="color:#505050;font-size:14px;" type="submit" name="pay_now" id="pay_now">PAYMENT</button>
</form>
</li>-->						</ul>
					</nav>
				</div>
			</div>
		</div>
	</div>
</div> <!-- menu.php file included in topbar.php file -->
</header><div class="request-button top-1" onClick="parent.location='email_order.php'">
		<span>Price Request</span>
</div>
<div class="request-button top-2" onClick="parent.location='get_offer_lab_diamond.php'">
		<span>Make an Offer</span>
</div>

<div class="page-title">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<ul class="breadcrumbs">
					<li><a href="index.php">Home</a> </li>
					<li class="separator"></li>
					<!--<li>Shop</li>
					<li class="separator"></li>-->
					<li>Loose Lab Diamonds</li>
					
				</ul>
			</div>
		</div>
	</div>
</div>

<style>
#main_div p {
	margin: 0 0 5px;
	text-align: justify;
	font-weight: 500;
}
.shape-desc {
    background: #ffd1dc1f;
    padding: 10px 10px;
    border-radius: 10px;
    margin: 7px;
    width: 99%;
	border: 1px dashed #d5d5d5;
}
</style>


<!-- Page Content-->
<div class="container padding-bottom-3x mb-1">
	<div class="row g-3">
		<div class="col-lg-12 order-lg-12" id="list_view_ajax">
			<div class="row g-3" id="main_div">				
				<div class="col-xxl-3 col-md-4 col-6">
		<a class="product-button" href="email_order.php?shape=Cushion">
		<div class="product-card">
			<div class="product-thumb">
			
				<img class="lazy" alt="Product" src="images/shapes/cushion.jpg" onerror="this.onerror=null; this.src='assets/noproduct.png'" 
					style="width:auto;height: auto">
				
			</div>
			<div class="product-card-body">
				<p  style="color:#727272;text-align:center;text-transform:uppercase">Cushion</p>
			</div>
		</div>
		</a>
	</div><div class="col-xxl-3 col-md-4 col-6">
		<a class="product-button" href="email_order.php?shape=Emerald">
		<div class="product-card">
			<div class="product-thumb">
			
				<img class="lazy" alt="Product" src="images/shapes/emerald.jpg" onerror="this.onerror=null; this.src='assets/noproduct.png'" 
					style="width:auto;height: auto">
				
			</div>
			<div class="product-card-body">
				<p  style="color:#727272;text-align:center;text-transform:uppercase">Emerald</p>
			</div>
		</div>
		</a>
	</div><div class="col-xxl-3 col-md-4 col-6">
		<a class="product-button" href="email_order.php?shape=Marquise">
		<div class="product-card">
			<div class="product-thumb">
			
				<img class="lazy" alt="Product" src="images/shapes/marquise.jpg" onerror="this.onerror=null; this.src='assets/noproduct.png'" 
					style="width:auto;height: auto">
				
			</div>
			<div class="product-card-body">
				<p  style="color:#727272;text-align:center;text-transform:uppercase">Marquise</p>
			</div>
		</div>
		</a>
	</div><div class="col-xxl-3 col-md-4 col-6">
		<a class="product-button" href="email_order.php?shape=Oval">
		<div class="product-card">
			<div class="product-thumb">
			
				<img class="lazy" alt="Product" src="images/shapes/oval.jpg" onerror="this.onerror=null; this.src='assets/noproduct.png'" 
					style="width:auto;height: auto">
				
			</div>
			<div class="product-card-body">
				<p  style="color:#727272;text-align:center;text-transform:uppercase">Oval</p>
			</div>
		</div>
		</a>
	</div>				<div class="shape-desc">
					<p style="color:#919191">Prices - 1ct to1.4 ct - $ 700 per ct, 1.5 ct to 1.8 ct -$800 per ct, 2ct to 2.6 ct $1000 per ct, 3ct to 3.75 ct $1200 per ct, 4 ct + -$1500 per ct .If you are getting lower prices than these please click on the best offer button and give  your best offer for the colour,clarity and stone size required. 
					  <font color="#636363">After you make an offer, we can choose to accept, decline, or make a counteroffer. No payment details are required at the time of making the offer.</font>
				   </p>
					<p style="color:#919191">All stones are certified by IGI or GIA and each stone is inscribed with the certificate number and sold along with the original certificate. The above prices of stones are for G colour , Vs1 clarity and  Excellent Cut.</p>
				</div>
				<div class="col-xxl-3 col-md-4 col-6">
		<a class="product-button" href="email_order.php?shape=Pear">
		<div class="product-card">
			<div class="product-thumb">
			
				<img class="lazy" alt="Product" src="images/shapes/pear.jpg" onerror="this.onerror=null; this.src='assets/noproduct.png'" 
					style="width:auto;height: auto">
				
			</div>
			<div class="product-card-body">
				<p  style="color:#727272;text-align:center;text-transform:uppercase">Pear</p>
			</div>
		</div>
		</a>
	</div><div class="col-xxl-3 col-md-4 col-6">
		<a class="product-button" href="email_order.php?shape=Princess">
		<div class="product-card">
			<div class="product-thumb">
			
				<img class="lazy" alt="Product" src="images/shapes/princess.jpg" onerror="this.onerror=null; this.src='assets/noproduct.png'" 
					style="width:auto;height: auto">
				
			</div>
			<div class="product-card-body">
				<p  style="color:#727272;text-align:center;text-transform:uppercase">Princess</p>
			</div>
		</div>
		</a>
	</div><div class="col-xxl-3 col-md-4 col-6">
		<a class="product-button" href="email_order.php?shape=Radiant">
		<div class="product-card">
			<div class="product-thumb">
			
				<img class="lazy" alt="Product" src="images/shapes/radiant.jpg" onerror="this.onerror=null; this.src='assets/noproduct.png'" 
					style="width:auto;height: auto">
				
			</div>
			<div class="product-card-body">
				<p  style="color:#727272;text-align:center;text-transform:uppercase">Radiant</p>
			</div>
		</div>
		</a>
	</div><div class="col-xxl-3 col-md-4 col-6">
		<a class="product-button" href="email_order.php?shape=Round">
		<div class="product-card">
			<div class="product-thumb">
			
				<img class="lazy" alt="Product" src="images/shapes/round.jpg" onerror="this.onerror=null; this.src='assets/noproduct.png'" 
					style="width:auto;height: auto">
				
			</div>
			<div class="product-card-body">
				<p  style="color:#727272;text-align:center;text-transform:uppercase">Round</p>
			</div>
		</div>
		</a>
	</div>				<div class="shape-desc">
					<p style="color:#919191">For more information click on the stone shape then click on  size required ,add  your email /phone number and our sales team will connect with you.</p>
					<p style="color:#919191">Other colours , clarity and sizes are available on request.</p>
				</div>
			</div>
		</div>
	</div>
</div>

<br><br><br><br>
<!-- <section class="brand-section mt-30 mb-60">
    <div class="container ">
        <div class="row">
            <div class="col-lg-12 ">
                <div class="section-title">
                    <h2 class="h3">Popular Categories</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="brand-slider owl-carousel">
                    <div class="slider-item">
                        <a class="text-center" href="http://demomirtex.siscotech.com/home/catalog.php?cat_id=5">
                        <img class="d-block hi-50 lazy"
                            src="https://www.giftsinternational.net/images/knowledge-hub/history-of-christmas-presents1.jpg?width=600&format=webp"
                            alt="" title="">
                        </a>
                    </div>                   
                    <div class="slider-item">
                        <a class="text-center" href="http://demomirtex.siscotech.com/home/catalog.php?cat_id=7">
                        <img class="d-block hi-50 lazy"
                            src="https://cdn.vox-cdn.com/thumbor/5JZLO-c2_DTwLiCqHCHnKllFyj0=/1400x1050/filters:format(jpeg)/cdn.vox-cdn.com/uploads/chorus_asset/file/9850305/OneVanguardWay_DallasTX_Dec17_90.jpg"
                            alt="" title="">
                        </a>
                    </div>
                    <div class="slider-item">
                        <a class="text-center" href="http://demomirtex.siscotech.com/home/catalog.php?cat_id=9">
                        <img class="d-block hi-50 lazy"
                            src="https://media.istockphoto.com/id/1336644071/photo/mans-hand-opening-the-toilet-lid.jpg?s=612x612&w=0&k=20&c=yE_AdYxZeqR-UWGhyK-f-AlcLW_oTuRURUfv4TjU524="
                            alt="" title="">
                        </a>
                    </div>
                    <div class="slider-item">
                        <a class="text-center" href="http://demomirtex.siscotech.com/home/catalog.php?cat_id=10">
                        <img class="d-block hi-50 lazy"
                            src="https://media.istockphoto.com/id/1161177015/photo/modern-living-room-in-natural-botanical-style.jpg?s=612x612&w=0&k=20&c=h5oNCdES_R68ZbIfA8ez6PDZIBzJJPTulgXGF6WJEIE="
                            alt="" title="">
                        </a>
                    </div>
                    <div class="slider-item">
                        <a class="text-center" href="http://demomirtex.siscotech.com/home/catalog.php?cat_id=8">
                        <img class="d-block hi-50 lazy"
                            src="https://cdn3.vectorstock.com/i/1000x1000/04/92/religious-word-text-typography-design-logo-icon-vector-21490492.jpg"
                            alt="" title="">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> -->
<!--    announcement banner section start   -->
<!-- <a class="announcement-banner" href="#announcement-modal"></a>
<div id="announcement-modal" class="mfp-hide white-popup">
            <div class="announcement-with-content">
            <div class="left-area">
                <img src="../assets/images/1638791990Untitled-1.jpg" alt="">
            </div>
            <div class="right-area">
                <h3 class="">Get 50% Discount.</h3>
                <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Exercitationem, facere nesciunt doloremque nobis debitis sint?</p>
                <form class="subscriber-form" action="https://geniusdevs.com/codecanyon/omnimart40/subscriber/submit" method="post">
                    <input type="hidden" name="_token" value="LRzXoyBjePB3fmlu93rAz5Io5VY8VvTXujFsefkz">                    <div class="input-group">
                        <input class="form-control" type="email" name="email" placeholder="Your e-mail">
                        <span class="input-group-addon"><i class="icon-mail"></i></span> </div>
                    <div aria-hidden="true">
                        <input type="hidden" name="b_c7103e2c981361a6639545bd5_1194bb7544" tabindex="-1">
                    </div>

                    <button class="btn btn-primary btn-block mt-2" type="submit">
                        <span>Subscribe</span>
                    </button>
                </form>
            </div>
        </div> 
</div> -->

<!--    announcement banner section end   -->
<!-- Site Footer-->
<footer class="site-footer">
    <div class="container">
      <div class="text-center">
       <!--  <span style="font-size: 16PX;font-weight: 500;color: gray;">All diamonds are GIA and IGI certified.</span><br>-->
          <span style="font-size: 12PX;font-weight: 500">LAB-GROWN DIAMONDS ARE NOW BECOMING A POPULAR ALTERNATIVE FOR MANY PEOPLE,</span><br>
          <span style="font-size: 12PX;font-weight: 500">SINCE THEY CAN HAVE THE SAME BEAUTY AND PROPERTIES AS A MINED DIAMOND AND ARE OFTEN MORE AFFORDABLE</span><br><br><br>
        </div>
      <div class="row">  
        <div class="col-lg-3 col-md-6">
          <!-- Contact Info-->
          <section class="widget widget-light-skin">
                <h3 class="widget-title">Get In Touch</h3>
                <p class="mb-1">38 W48 Street, #203 <br> New York, NY 10036</p>
                <p class="mb-1"><strong>t: </strong> (212) 381-8822</p>
                <p class="mb-1"><strong>c: </strong> (646) 645-5626</p>
                <p class="mb-1"><strong>f: </strong> (646) 822-2438</p>
                <p class="mb-1"><strong>e: </strong> sam@trilliantdiam.com</p>
                <br>
                <h6>Follow Us On</h6>
                <div class="footer-social-links">
                        <a href="https://www.facebook.com/profile.php?id=100085395053915&mibextid=ZbWKwL" target="_blank"><span><i class="fab fa-facebook-f"></i></span></a>
                        <a href="https://twitter.com/" target="_blank"><span><i class="fab fa-twitter"></i></span></a>
                        <!-- <a href="https://www.youtube.com/"><span><i class="fab fa-youtube"></i></span></a> -->
                        <a href="https://www.instagram.com/" target="_blank"><span><i class="fab fa-instagram"></i></span></a>
                    </div>
                        <!-- <p>Download the Trilliant Diamond App Now</p>
                    <div class="row" style="max-width: 30%;height: auto;vertical-align: middle;" >
                        <a href="https://play.google.com/store/apps/details?id="><img src="assets/images/footer/google-play.svg"></a>
                        <a href="https://apps.apple.com/"><img src="assets/images/footer/apple.svg"></a>
                    </div> -->
          </section>
        </div>
        <div class="col-lg-3 col-sm-6">
          <!-- Customer Info-->
          <div class="widget widget-links widget-light-skin">
            <h3 class="widget-title">Informations</h3>
            <ul>
                    <!-- <li> <a href='pages.php?page=delivery_information'>Delivery Information</a> </li>
                    <li> <a href='pages.php?page=international_shipping'>International Shipping</a> </li>
                    <li> <a href='pages.php?page=payment_option'>Payment Options</a> </li>
                    <li> <a href='pages.php?page=track_order'>Track Your Order</a> </li> -->
                    <li> <a href='pages.php?page=shipping-policy'>Shipping Policy</a> </li>
                    <li> <a href='pages.php?page=return_policy'>Return Policy</a> </li>
                    <li> <a href='pages.php?page=faq'>FAQ</a> </li>
            </ul>
          </div>
        </div>
        <div class="col-lg-3 col-sm-6">
          <!-- Customer Info-->
          <div class="widget widget-links widget-light-skin">
            <h3 class="widget-title">Usefull Links</h3>
            <ul>

                <li> <a href='index.php'>Loose Lab Diamonds</a> </li>
                <li> <a href='meleeList.php'>Melee Lab Diamonds</a> </li>
                <li> <a href='pages.php?page=about-us'>About Us</a> </li>
                <!-- <li> <a href='pages.php?page=shipping-policy'>Shipping Policy</a> </li>
                <li> <a href='pages.php?page=term-of-use'>Terms Of Use</a> </li> -->
                <!-- <li> <a href='catalog.php?cat_id=34'>Rings</a> </li><li> <a href='catalog.php?cat_id=5'>Daimond</a> </li><li> <a href='catalog.php?cat_id=8'>Wedding Rings</a> </li><li> <a href='catalog.php?cat_id=4'>Loose Daimonds</a> </li><li> <a href='catalog.php?cat_id=7'>Engagement Rings</a> </li><li> <a href='catalog.php?cat_id=10'>Jewelry</a> </li><li> <a href='catalog.php?cat_id=9'>Ear Rings</a> </li> -->
            </ul>
          </div>
        </div>
       
        <div class="col-lg-3">
            <!-- Subscription-->
            <section class="widget">
              <h3 class="widget-title">Newsletter</h3>
              <form  id="newsLetter" class="row" method="post">
                <input type="hidden" name="_token" value="LRzXoyBjePB3fmlu93rAz5Io5VY8VvTXujFsefkz">
                <div class="col-sm-12">
                  <div class="input-group">
                    <input class="form-control" type="email" name="email" placeholder="Your e-mail" required>
                    <span class="input-group-addon"><i class="icon-mail"></i></span> </div>
                  <div aria-hidden="true">
                    <input type="hidden" name="b_c7103e2c981361a6639545bd5_1194bb7544" tabindex="-1">
                  </div>

                </div>
                <div class="col-sm-12">
                  <button class="btn btn-primary btn-block mt-2" type="submit" style="background-color: #c88393 !important;">
                      <span>Subscribe</span>
                  </button>
                </div>
                <div class="col-lg-12">
                    <p class="text-sm opacity-80 pt-2">Subscribe to our Newsletter to receive early discount offers, latest news, sales and promo information.</p>
                </div>
              </form>
              <!--<div class="pt-3"><img class="d-block gateway_image" src="assets/images/16305963101621960148credit-cards-footer.png"></div>-->
              <!--<div class="pt-3" style="text-align: -webkit-center;"><img class="d-block gateway_image" src="assets/images/jbt.jpg" style="width: 93px;"></div>-->
            </section>
          </div>
      </div>
      <!-- Copyright-->
      <p class="footer-copyright"> 2023  ©  Trilliant Diamond Limited. All Rights Reserved.</p>
    </div>
  </footer>

<!-- Back To Top Button-->
<a class="scroll-to-top-btn" href="#">
    <i class="icon-chevron-up"></i>
</a>

<!-- <div class="color-picker">
    <span class="spiner"><i class="fas fa-cog fa-spin"></i></span>
    <a href="javascript:;" class="color__check " style="background: #FF6A00;" data-href="FF6A00"></a>
    <a href="javascript:;" class="color__check " style="background: #ff4719;" data-href="ff4719"></a>
    <a href="javascript:;" class="color__check " style="background: #03a9f4;" data-href="03a9f4"></a>
    <a href="javascript:;" class="color__check " style="background: #A120CB;" data-href="A120CB"></a>
    <a href="javascript:;" class="color__check " style="background: #12A05C;" data-href="12A05C"></a>
    <a href="javascript:;" class="color__check " style="background: #000;" data-href="000"></a>
</div> -->

<!-- Backdrop-->
<div class="site-backdrop"></div>
<!-- Cookie alert dialog  -->
<!--
<div class="js-cookie-consent cookie-consent">
    <div class="container">
        <div class="cookie-container">
          <span class="cookie-consent__message">
            Your experience on this site will be improved by allowing cookies.
          </span>

          <button class="btn btn-info js-cookie-consent-agree cookie-consent__agree">
              Allow Cookies
          </button>
        </div>
      </div>
</div>
-->
<!-- Cookie alert dialog  -->
<script>
    var mainbs = {"is_announcement":"1","announcement_delay":"1.00","overlay":null};
    var decimal_separator = '.';
    var thousand_separator = ',';
</script>
<!-- JavaScript (jQuery) libraries, plugins and custom scripts-->
<script type="text/javascript" src="assets/front/js/plugins.min.js"></script>
<script type="text/javascript" src="assets/back/js/plugin/bootstrap-notify/bootstrap-notify.min.js"></script>
<script type="text/javascript" src="assets/front/js/scripts.min.js"></script>
<script type="text/javascript" src="assets/front/js/lazy.min.js"></script>
<script type="text/javascript" src="assets/front/js/lazy.plugin.js"></script>
<script type="text/javascript" src="assets/front/js/myscript.js"></script>

<script type="text/javascript">
    let mainurl = 'http://trilliantdiam.siscotech.com/home';

    $(document).on('click','.color__check',function(){
        var color = $(this).attr('data-href');
        $.get(`${mainurl}/set/color?color=${color}`,function(){
          location.reload();
        });
    })
    $(document).on('click','.spiner',function(){
        $('.color-picker').toggleClass('active');
    })

    let view_extra_index = 0;
      // Notifications
      function SuccessNotification(title){
            $.notify({
                title: ` <strong>${title}</strong>`,
                message: '',
                icon: 'fas fa-check-circle'
                },{
                element: 'body',
                position: null,
                type: "success",
                allow_dismiss: true,
                newest_on_top: false,
                showProgressbar: false,
                placement: {
                    from: "top",
                    align: "right"
                },
                offset: 20,
                spacing: 10,
                z_index: 1031,
                delay: 5000,
                timer: 1000,
                url_target: '_blank',
                mouse_over: null,
                animate: {
                    enter: 'animated fadeInDown',
                    exit: 'animated fadeOutUp'
                },
                onShow: null,
                onShown: null,
                onClose: null,
                onClosed: null,
                icon_type: 'class'
            });
        }

        function DangerNotification(title){
            $.notify({
                // options
                title: ` <strong>${title}</strong>`,
                message: '',
                icon: 'fas fa-exclamation-triangle'
                },{
                // settings
                element: 'body',
                position: null,
                type: "danger",
                allow_dismiss: true,
                newest_on_top: false,
                showProgressbar: false,
                placement: {
                    from: "top",
                    align: "right"
                },
                offset: 20,
                spacing: 10,
                z_index: 1031,
                delay: 5000,
                timer: 1000,
                url_target: '_blank',
                mouse_over: null,
                animate: {
                    enter: 'animated fadeInDown',
                    exit: 'animated fadeOutUp'
                },
                onShow: null,
                onShown: null,
                onClose: null,
                onClosed: null,
                icon_type: 'class'
            });
        }
        // Notifications Ends
    </script>
    <script>
        $(document).on('submit', '#newsLetter', function(event){
        event.preventDefault();
            
        $("html, body").animate({
            scrollTop: 0
        }, 800);		
        
        $.ajax({
            url:"fetch.php?postId=newsLetter",
            method:'POST',
            data:new FormData(this),
            contentType:false,
            processData:false,
            success:function(data)
            {
             //successNotification('tt');            
                alert('Successfully subscribed the newsletter!');
            }
        });
        });

</script>
<div class="modal fade" id="customerLoginModal" tabindex="-1"  aria-hidden="true">
    <form action="#" id="customer_login" method="POST">
        <input type="hidden" name="_token" value="doCo7x7LscOUHivSXyviX8k5TGC24KxGnmunsElv">
        <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title">SIGN IN</h6>
          <button class="close" type="button" data-bs-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        </div>
        <div class="modal-body">
          <div class="card-body">
            <div class="col-lg-12 form-group">
              <input class="form-control" type="email" name="cus_email" placeholder="Email" value="" required>
            </div>
            <div class="col-lg-12 form-group">
              <input class="form-control" type="password" name="cus_password" placeholder="Password" required>
            </div>           
            </div>
            <div class="col-lg-12  form-group" style="padding-left: 140px;">                
                <div data-sitekey="6LcnPoEaAAAAAF6QhKPZ8V4744yiEnr41li3SYDn" class="g-recaptcha"></div>
                <script src="https://www.google.com/recaptcha/api.js?" async defer></script>
                <div id="g-recaptcha-error"></div>
            </div>
        </div>        
        <div class="modal-footer">
          <!-- <button class="btn btn-primary" type="button" data-bs-dismiss="modal"><span>Cancel</span></button> -->
          <button class="btn btn-primary margin-bottom-none" type="submit"><span>Login</span></button>       
        </div>
      </div>
    </div>
    </form>
  </div>

  <div class="modal fade" id="customerRegistrationModal" tabindex="-1"  aria-hidden="true">
  <form action="#" id="customer_registration" method="POST">
        <input type="hidden" name="_token" value="doCo7x7LscOUHivSXyviX8k5TGC24KxGnmunsElv">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title">REGISTRATION FORM</h6>
          <button class="close" type="button" data-bs-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        </div>
        <div class="modal-body">
          <div class="card-body">
          <div class="col-sm-12">
                <div class="form-group">
                  <label for="reg-fn">Name</label>
                  <input class="form-control" type="text" name="company_name" placeholder="Company Name" required>
                </div>
              </div>
              <div class="col-sm-12">
                <div class="form-group">
                  <label for="reg-ln">Address</label>
                  <input class="form-control" type="text" name="street" placeholder="Street"  value="">
                </div>
              </div>
              <div class="col-sm-12">
                <div class="form-group">
                  <label for="reg-email">City</label>
                  <input class="form-control" type="City" name="city" placeholder="City" value="">
                </div>
              </div>
              <div class="col-sm-12">
                <div class="form-group">
                  <label for="reg-phone">State</label>
                  <input class="form-control" name="state" type="text" placeholder="State"  value="">
                </div>
              </div>
              <div class="col-sm-12">
                <div class="form-group">
                  <label for="reg-email">Zip</label>
                  <input class="form-control" type="text" name="zip" placeholder="Zip" value="">
                </div>
              </div>
              <div class="col-sm-12">
                <div class="form-group">
                  <label for="reg-phone">Contact Person</label>
                  <input class="form-control" name="contact_person" type="text" placeholder="Contact Person" value="">
                </div>
              </div>
              <div class="col-sm-12">
                <div class="form-group">
                  <label for="reg-phone">Phone Number</label>
                  <input class="form-control" name="phone" type="text" placeholder="Phone Number"  required>
                </div>
              </div>
              <div class="col-sm-12">
                <div class="form-group">
                  <label for="reg-email">E-mail Address</label>
                  <input class="form-control" type="email" name="email" placeholder="This email be used for login"  required>
                </div>
              </div>
              <!-- <div class="col-sm-6">
                <div class="form-group">
                  <label for="reg-phone">Years Of Doing Buisness</label>
                  <input class="form-control" name="years_of_doing_buisness" type="text" placeholder="Years Of Doing Buisness" value="">
                </div>
              </div> -->
              <input name="years_of_doing_buisness" type="hidden" value="0">         
              <div class="col-sm-12">
                <div class="form-group">
                  <label for="reg-pass">Password</label>
                  <input class="form-control" type="password" name="password" placeholder="Password">
                </div>

              </div>
              <div class="col-sm-12">
                <div class="form-group">
                  <label for="reg-pass-confirm">Confirm Password</label><output name="result" style="display:none"><i class="entypo-cancel"></i> </output>
                  <input class="form-control" type="password" name="password_confirmation" placeholder="Confirm Password">
                </div>
              </div>

               <div class="col-lg-12 mb-4" style="padding-left: 130px;">
                  <script src="https://www.google.com/recaptcha/api.js?" async defer></script>
                <div data-sitekey="6LcnPoEaAAAAAF6QhKPZ8V4744yiEnr41li3SYDn" class="g-recaptcha"></div>
                <div id="g-recaptcha-error"></div>
              </div>        
            </div>
        </div>        
        <div class="modal-footer">
          <!-- <button class="btn btn-primary" type="button" data-bs-dismiss="modal"><span>Cancel</span></button> -->
          <button class="btn btn-primary margin-bottom-none registration" type="submit"><span>Register</span></button>     
        </div>
      </div>
    </div>
    </form>
  </div>
  <script>
  $(document).on('submit', '#customer_login', function(event){
      event.preventDefault();

      var googleResponse = $('#g-recaptcha-response').val();
      if(googleResponse==''){   
          $("#g-recaptcha-error").html("<span style=color:red>Please check reCaptcha to continue.</span>");
          return false;
      }

      $("html, body").animate({
        scrollTop: 0
      }, 800);		
      
      $.ajax({
        url:"fetch.php?postId=login",
        method:'POST',
        data:new FormData(this),
        contentType:false,
        processData:false,
        success:function(data)
        {          
          if(data=='1'){
                window.location.href = 'client_dashboard.php';
          }else{
            DangerNotification(data);
            
          }          
        }
      });
	});
</script>
<script>
 $(document).on('submit', '#customer_registration', function(event){
      event.preventDefault();

      var googleResponse = $('#g-recaptcha-response').val();
      if(googleResponse==''){   
          $("#g-recaptcha-error").html("<span style=color:red>Please check reCaptcha to continue.</span>");
          return false;
      }

      $("html, body").animate({
        scrollTop: 0
      }, 800);		
      
      $.ajax({
        url:"fetch.php?postId=registration",
        method:'POST',
        data:new FormData(this),
        contentType:false,
        processData:false,
        success:function(data)
        {
          SuccessNotification(data);
        }
      });
	});
</script></body>
</html>

BITM;

echo $myString;
