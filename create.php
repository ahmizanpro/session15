<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>BOOTSTRAP FORM</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>

<body>
    <form action="process.php" method="post">
        <div class="container" style="padding:40px 0px; background: green; color:white; font-size: 20px; font-weight:bold;">
            <div class="col-lg-6 col-lg-offset-3">
                <h2 style="font-size: 49px; padding: 30px 0px;">
                    Person Information Form
                </h2>
            </div>
            <div class="col-lg-8 col-lg-offset-2">
                <div class="form-group" style="padding: 10px 0px;">
                    <label for="firstName">First Name</label>
                    <input style="padding: 25px 10px;" class="form-control" type="text" name="firstName" id="firstName" placeholder="first name" required>
                </div>
                <div class="form-group" style="padding: 10px 0px;">
                    <label for="lastName">Last Name</label>
                    <input style="padding: 25px 10px;" class="form-control" type="text" name="lastName" id="lastName" placeholder="last name" required>
                </div>
                <div class="form-group" style="padding: 10px 0px;">
                    <label for="NID">NID</label>
                    <input style="padding: 25px 10px;" class="form-control" type="number" name="NID" id="NID" placeholder="identity number" required>
                </div>
                <div class="form-group" style="padding: 10px 0px;">
                    <label for="bloodGroup">Blood Group</label>
                    <input style="padding: 25px 10px;" class="form-control" type="text" name="bloodGroup" id="bloodGroup" placeholder="blood group" required>
                </div>
                <div class="form-group" style="padding: 10px 0px;">
                    <input class="btn btn-default btn-lg" style="padding:16px 40px; color:black; font-size: 24px; font-weight:bold;" type="submit" value="Submit">
                </div>
            </div>

        </div>

    </form>
</body>

</html>