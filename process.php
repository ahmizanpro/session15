<?php

/**
 * I've to apply the $_SESSION superglobal variable for remind the inputed data from the another page
 * First of all session will be start by this syntax
 */
if (!isset($_SESSION)) session_start();
// echo "<pre>";
// var_dump($_POST);
// echo "</pre>";

//bellow this syntax written for not showing the error message from the process.php page
if (isset($_POST['firstName'])) {
    $_SESSION['firstName'] = $_POST['firstName'];
    $_SESSION['lastName'] = $_POST['lastName'];
    $_SESSION['NID'] = $_POST['NID'];
    $_SESSION['bloodGroup'] = $_POST['bloodGroup'];
}


echo "<pre>";
var_dump($_SESSION);
echo "</pre>";

echo "<button type='button><a href='destroy.php'>Destroy Session</a></button>";
